import { VoiceChannel, VoiceConnection } from 'discord.js'
import * as ytdl from 'ytdl-core'
export class MusicClient {
  channel?: VoiceChannel
  stream?: any
  brodcast?: VoiceConnection
  playList?: Array<any>

  constructor (voiceChannel: VoiceChannel) {
    this.channel = voiceChannel
    this.playList = []
    this.joinChannel(this.channel)
  }

  disconectChannel () {
    if (this.brodcast !== undefined) {
      this.brodcast.disconnect()
    }
  }

  addStream (url: string) {
    console.log(this.brodcast)
    console.log(this.stream)
    if (this.brodcast && this.playList) {
      console.log('Addstream passed')
      this.playList.push(url)
      if (this.stream === undefined) {
        console.log('boop')
        this.playNext()
      }
    }
  }

  playStream (url: string) {
    if (this.brodcast !== undefined) {
      let stream = ytdl(url, { filter: 'audioonly' })
      this.stream = this.brodcast.playStream(stream, { volume: 0.5 })
      this.autoPlay()
    }
  }

  autoPlay () {
    if (this.stream) {
      this.stream.on('end', () => {
        this.playNext()
      })
      this.stream.on('error', (error: Error) => {
        console.error(error)
      })
    }
  }

  joinChannel (channel: VoiceChannel) {
    channel.join()
      .then(joinedChannel => {
        this.brodcast = joinedChannel
      })
  }

  playNext () {
    console.log('PlayNext passed')
    if (this.playList && !(this.playList.length === 0)) {
      let song = this.playList.pop()
      this.playStream(song)
    }
  }
}
