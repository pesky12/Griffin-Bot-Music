import { MusicClient } from './utils/musicClient'
import { Client, Collection, Message } from 'discord.js'

const client = new Client({ disableEveryone: true })
const musicCollection = new Collection()

client.on('ready', () => {
  console.log('Ready!')
})

client.on('message', (message) => {
  let currentClient: Object = findMusicClient(message)
  console.log(musicCollection)
  let handler = message.content.split(' ')
  let cmd = handler[0].slice(1)
  if (cmd === 'play') {
    currentClient.addStream(handler[1])
  }
  if (cmd === 'skip') {
    currentClient.playNext()
  }
  if (cmd === 'leave') {
    currentClient.disconectChannel()
    musicCollection.delete(message.guild.id)
  }
})
function findMusicClient (message: Message) {
  let find = musicCollection.get(message.guild.id)
  if (find) {
    console.log('Found it')
    console.log(find)
  } else {
    find = new MusicClient(message.member.voiceChannel)
    musicCollection.set(message.guild.id, find)
  }
  return find
}

client.login(process.env.DISCORD_TOKEN)
